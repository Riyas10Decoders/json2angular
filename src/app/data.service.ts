import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class DataService {
  private data = new BehaviorSubject('default message');
  currentProduct = this.data.asObservable();
  constructor() { }
  nextProduct(product: any) {
    this.data.next(product);
  }

}
