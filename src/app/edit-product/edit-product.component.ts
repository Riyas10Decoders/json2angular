import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {
  // data = this._dataService.getData();
  // id: any;
  // product: any;
  constructor(private httpService: HttpClient, private _dataService: DataService, private router: Router, private routes: ActivatedRoute) {


  }

  // arrProducts = [];
  // ngOnInit() {
  //   this.getProducts();
  //   this.routes.queryParams
  //     .subscribe(params => {
  //       this.id = params.id
  //       console.log('-------------------------------', params); // {order: "popular"}

  //     });

  // public product: {
  //   ID:'',
  //   Model: '',
  //   Name: '',
  //   Price: '',
  //   Product: ''
  // }
  public product : any;
  isEditable = [];

  ngOnInit() {
    this._dataService.currentProduct.subscribe(res => {
      console.log('response------', res)
      if(res!= "default message"){
      this.product = res
      for (let i = 0; i < this.product.length; i++) {
        this.isEditable[i] = false;
      }
      }
    });

    // this.product = this.res;
    // // console.log('-------', this.res);
    console.log('+++++', this.product.Model, this.product.Name,this.product.Price,this.product.Product );

  }
  edit(product,i) {
    this.isEditable[i] = true;
  console.log('Entering in edit------------------', product, i);
  
  // this.router.navigate(['/editproduct']);
  // this.router.navigate(['/editproduct'], { queryParams: { id: product.ID } });
  
}
save(product,i){
  console.log("++++++");
  this._dataService.nextProduct(product);
  this.isEditable[i] = false;
}
}


  // getProducts() {
  //   this.httpService.get('http://www.json-generator.com/api/json/get/bVunzXdeeW?indent=2').subscribe(
  //     data => {
  //       console.log('-------',data);
  //       this.arrProducts = data as string[];
  //       this.arrProducts.forEach(data =>{
  //       if(this.id == data.ID) {
  //        this.product = data;
  //       }
  //         console.log('-----------------------data----------',this.product)
  //       })


  // });
// }
// }
