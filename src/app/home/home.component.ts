import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { DataService } from '../data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {

  constructor(private httpService: HttpClient, private router: Router, private _dataService: DataService) { }
  arrProducts: string[];
  isEditable = [];

  ngOnInit() {
    this.httpService.get('http://www.json-generator.com/api/json/get/bVunzXdeeW?indent=2').subscribe(
      data => {
        this.arrProducts = data as string[];	 // FILL THE ARRAY WITH DATA.
        //  console.log(this.arrProducts[1]);

        for (let i = 0; i < this.arrProducts.length; i++) {
          this.isEditable[i] = false;
        }
      },
      (err: HttpErrorResponse) => {
        console.log(err.message);
      }
    );
  }
  edit(product, i) {
    this.isEditable[i] = true;
    console.log('Entering in edit------------------', product, i);
    this._dataService.nextProduct(product);
    this.router.navigate(['/editproduct']);
    // this.router.navigate(['/editproduct'], { queryParams: { id: product.ID } });

  }

}
